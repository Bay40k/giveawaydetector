giveaway
=========

Detects posts in /r/pcmasterrace with the flair "Giveaway" in an interval, sends email to owner notifying of giveaway.

Dependencies:

- PRAW (Reddit API Wrapper)