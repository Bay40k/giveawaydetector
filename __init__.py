import praw
import time
import config
from datetime import datetime
r = praw.Reddit('Python terminal') #user agent
startTime = datetime.now()

subredditVar = "Joe56780"
loopInterval = 10 #wait x seconds then loop

def main():
    global r
    global subredditVar
    
    oldTime = 1375258571 #I think this is just a big random number idk
    subreddit = r.get_subreddit(subredditVar)
    post_limit = 1 #idk what this is
    while True:
        for submission in subreddit.get_new(limit=post_limit):
            subtime=submission.created
            
            if subtime > oldTime:
                    oldTime = subtime

                    newSubmission = r.get_submission(submission.permalink + ".json")
                    print(newSubmission)
                    if newSubmission.link_flair_text == "Giveaway":
                        print("Giveaway flair detected!")
                        
                    else: # do this if flair is not giveaway
                        if newSubmission.link_flair_text == None: #if flair is none show this
                            print("Not a giveaway (Flair: None)")
                        else: #else
                            print("Not a giveaway (" + newSubmission.link_flair_text + ")")
        print("Checking again in " + str(loopInterval) + " seconds")
        time.sleep(loopInterval)
main()

#TODO send email, (easy)